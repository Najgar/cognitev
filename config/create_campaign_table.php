<?php
	include "meta-data.php";

	$conn = new mysqli($servername, $username, $password,$dbname);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$sql ="CREATE TABLE campaign (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	country VARCHAR(255) NOT NULL,
	budget INT(6) NOT NULL,
	goal VARCHAR(255) NOT NULL,
	category VARCHAR(255) NOT NULL,
	created_at DATETIME default CURRENT_TIMESTAMP
	)";

	if ($conn->query($sql) === TRUE) {
	    echo "Table created successfully";
	} else {
	    echo "Error creating Table: " . $conn->error;
	}

	$conn->close();
?>