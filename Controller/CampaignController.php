<?php
	class CampaignController{
		public function test(){
			echo "Hello World";
		}
		public function add(){
			include(__DIR__."/../API/CampaignService.php");
			try{
				$service = new CampaignService();

				if ((!isset($_GET['name']) || $_GET['name'] == '')){
					throw new Exception('Name Cannot be null');
				}
				$service->setName($_GET['name']);

				if ((!isset($_GET['country']) || $_GET['country'] == '')){
					throw new Exception('Country Cannot be null');
				}
				$service->setCountry($_GET['country']);

				if ((!isset($_GET['budget']) || $_GET['budget'] == '')){
					throw new Exception('Budget Cannot be null');
				}
				$service->setBudget($_GET['budget']);

				if ((!isset($_GET['goal']) || $_GET['goal'] == '')){
					throw new Exception('Goal Cannot be null');
				}
				$service->setGoal($_GET['goal']);

				if ((!isset($_GET['category']) || $_GET['category'] == '')){
					$url = "https://ngkc0vhbrl.execute-api.eu-west-1.amazonaws.com/api/";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url.'?url='.$name );
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 7);
					$result = curl_exec($ch);
					curl_close($ch);
					$jsoned_result = json_decode($result);
					$category = $jsoned_result->category->name;
					$service->setCategory($category);
				}
				else{
					$service->setCategory($_GET['category']);
				}
				
				$response = $service->add();

				if ($response["success"]){
					echo "Record Created Successfully";
				}else{
					throw new Exception($response["error"]);
				}
			}catch (Exception $e) {
				echo 'Error : '.$e->getMessage();
			}
		}

		public function getYaxisValues(){
			include(__DIR__."/../API/CampaignService.php");
			if (isset($_GET['yaxis_param'])){
				$service = new CampaignService();
				$response = $service->get($projection = $_GET['yaxis_param'],$whereClause=null,$groupColumn = $_GET['yaxis_param']);
				if($response["success"]){
					return json_encode($response['data']);
				}
			}
		}

		public function getBarGraphData(){
			include(__DIR__."/../API/CampaignService.php");
			if (isset($_GET['yaxis_param']) && isset($_GET['xaxis_param']) && isset($_GET['datefrom']) && isset($_GET['dateto'])){
				$service = new CampaignService();
				$projection = $_GET['xaxis_param'].",".$_GET['yaxis_param'].","."COUNT(".$_GET['yaxis_param'].") AS cnt";
				$groupColumn = $_GET['xaxis_param'].",".$_GET['yaxis_param'];
				$whereClause = null;
				$gotFrom = false;
				if ($_GET['datefrom'] != ""){
					$gotFrom = true;
					$whereClause = "date(created_at) >= '".$_GET['datefrom']."'";
				}
				if ($_GET['dateto'] != ""){
					if ($gotFrom){
						$whereClause = $whereClause." AND ";
					}else{
						$whereClause = "";
					}
					$whereClause = $whereClause."date(created_at) <= '".$_GET['dateto']."'";
				}
				$response = $service->get($projection,$whereClause,$groupColumn);
				if($response["success"]){
					return json_encode($response['data']);
				}
			}
		}
	}
?>