# Campaign Analyser (Cognitev TECHNICAL Task) 

Hi! This is Abdurhman M. An-naggar here, I've developed this task in native php "To be easily installed on any device", I was looking forward to set a lot of features of it but I'm constrained to the time margin. So I hope this is worthy :smile:

# Structure

I've tried as much as I can to keep the sense of Frameworks in this native masterpiece :grin:, So as if you checked the repository you'll see the following:
* API
* assets
* config
* controller
* DBFunction

I'll Discuss each folder of this in the coming points.

## API

This one is responsible for all project services, in our case the campaign insertion or listing is done through the service files in this folder.

It's no responsible for redirection and handling input coming from user it's just setting that input coming from controller in the **DBProcedures** in order to fetch campaigns or add campaigns.

## DBFunction

All of the selection queries, insertion or update queries are implemented here in such abstract manner trying to make most of transactions follow the set,get,update,delete style. In our case we've not needed special queries, but if later needed we could implement it in the model as a procedure then consume it from the services in the API folder.

## config

Here we've the migrations of the database which keep us on the same line without a lot of confusion about the database structure from place to place, also we handle here the meta-data shared between project files.

> Whenever you clone the project you must run the database migration files in this folder in order to create the database and tables and relations between them, keep in mind you should check the **meta-data.php** file params (i.e. ServerName, DB username & DB password) in order to ensure the integrity of migration transactions


## controller

This is the bridge between the views and the services, here we handle the request parameters and types, and check the validity of sent data, almost this is responsible of redirection and validation.

## assets

This folder contains the js,css and img files needed in the project. this folder is UI oriented, it keeps all the user interface resources.


# Installation

1. Download [XAMPP](https://www.apachefriends.org/download.html) or whatever Apache Server you want.
2. Get sure that you don't have with Apache ports (80,443) or even mysql ports.
3. Clone the project
4. Modify **meta-data.php** file to meet your server configuration.
5. Run PHP scripts in config folder.
6. WHOAAAA, Your project is up.

## How To add a campaign

Now you can add campaigns by calling **add-campaign.php**. the GET params are:
* name
* country
* budget
* goal
* category "Not Mandatory"

## How To Visualise the campaigns

Navigate to **get-stats.php**, and taste the Magic :stuck_out_tongue_winking_eye::stuck_out_tongue_winking_eye:

## What's next

* I'm looking forward to implement pretty URLs using htaccess to make site navigation easy
* Apply something like **ORM eloquent** in laravel framework, in order to make the database manipulation reliable.

