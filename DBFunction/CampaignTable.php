<?php
	$myRoot = $_SERVER["DOCUMENT_ROOT"];
	class CampaignTable{
		protected $conn;
		function __construct() {
			include(__DIR__."/../config/meta-data.php");
	        $this->conn = new mysqli($servername, $username, $password, $dbname);
			// Check connection
			if ($this->conn->connect_error) {
			    die("Connection failed: " . $this->conn->connect_error);
			} 
	    }

	    public function insert($data){
	    	$response = array();
	    	if (count($data) == 0){
	    		echo "There's no fields specified for insertion in Campaign Table";
	    	}else{
	    		$sqlHead = "INSERT INTO campaign (";
	    		$sqlTail = "VALUES (";
	    		$counter = 0;
		    	foreach ($data as $key => $value){
		    		if ($counter > 0){
		    			$sqlHead = $sqlHead . ",";
		    			$sqlTail = $sqlTail . ",";
		    		}

		    		$sqlHead = $sqlHead . $key;
		    		$sqlTail = $sqlTail . "'" . $value . "'";
		    		$counter++;
		    	}
		    	$sqlHead = $sqlHead . ")";
		    	$sqlTail = $sqlTail . ")";
		    	
		    	$sql = $sqlHead." ".$sqlTail;

		    	if ($this->conn->query($sql) === TRUE) {
		    		$response["success"] = true;
		    		$response["error"]= "";
				} else {
				    //echo "Error: " . $sql . "<br>" . $conn->error;
				    $response["success"] = false;
		    		$response["error"]= $conn->error;
				}

				$this->conn->close();
				return $response;
	    	}
	    }

	    public function select($projection,$whereClause,$groupColumn){
	    	$sql = "SELECT ";
	    	if ($projection == null){
	    		$sql = $sql . "* ";
	    	}else{
	    		$sql = $sql . $projection . " ";
	    	}

	    	$sql = $sql . "FROM campaign";
	    	if ($whereClause != null){
	    		$sql = $sql . " WHERE ".$whereClause;
	    	}

	    	if ($groupColumn != null){
	    		$sql = $sql . " GROUP BY ".$groupColumn;
	    	}
	    	
	    	$result = $this->conn->query($sql);

	    	$response["success"] = true;
	    	$response["data"]= mysqli_fetch_all($result,MYSQLI_ASSOC);

			$this->conn->close();
			return $response;
	    }
	}