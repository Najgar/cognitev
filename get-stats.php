<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/selectize/css/selectize.default.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="assets/selectize/js/standalone/selectize.js"></script>
		<title>Campaign Filter</title>
		<script src="assets/chartjs/Chart.min.js"></script>
		<script src="assets/chartjs/utils.js"></script>

		<style>
			body{
				background: #c7e1f9;
			}
			canvas {
				-moz-user-select: none;
				-webkit-user-select: none;
				-ms-user-select: none;
			}
		</style>

		<script>
			var fieldsValue = ["name","country","category","goal","budget"];
			var fieldsText = ["Name","Country","Category","Goal","Budget"];

			$(document).ready(function(){
				$( "#datepicker-from" ).datepicker({ dateFormat: 'yy-mm-dd' });
				$( "#datepicker-to" ).datepicker({ dateFormat: 'yy-mm-dd' });
				$('#select-xaxis').selectize({
					placeholder: 'Select Basis (X-Axis)',
				});

				var yaxis = $('#select-yaxis').selectize({
					placeholder: 'Select Dependency (Y-Axis)',
				});
				yaxis[0].selectize.disable();

				var yaxisval = $('#select-yaxis-val').selectize({
					placeholder: 'Which Value in Y-Axis to display',
				});
				yaxisval[0].selectize.disable();
			});

			function drawGraph(xaxis,yaxix,xydata){
				if (window.myBar != null){
					window.myBar.destroy();
				}
				var color = Chart.helpers.color;
				var dataset = [];
				for (var i=0;i<yaxis.length;i++){
					var temp = [];
					for(var j=0;j<xydata.length;j++){
						temp.push(xydata[j][i]);
					}
					var colorNames = Object.keys(window.chartColors);
					var colorName = colorNames[dataset.length % colorNames.length];
					var dsColor = window.chartColors[colorName];
					dataset.push({
						label: yaxis[i],
						backgroundColor: color(dsColor).alpha(0.5).rgbString(),
						borderColor: dsColor,
						borderWidth: 1,
						data: temp,
					});
				}
				var barChartData = {
					labels: xaxis,
					datasets: dataset,
				};

				var ctx = document.getElementById('canvas').getContext('2d');
				window.myBar = new Chart(ctx, {
					type: 'bar',
					data: barChartData,
					options: {
						responsive: true,
						legend: {
							position: 'top',
						},
						title: {
							display: false,
						}
					}
				});
			}

			function onXaxisChange(){
				var yaxis = $('#select-yaxis')[0].selectize;
		        yaxis.disable();

				var yaxisval = $('#select-yaxis-val')[0].selectize;
		        yaxisval.disable();

		        var xaxisSelector = $('#select-xaxis')[0].selectize;
		        var selectedXaxisID = xaxisSelector.getValue();

		        yaxis.clear();
		        yaxis.clearOptions();

		        if (selectedXaxisID != ""){
		        	for(var i=0;i<fieldsValue.length;i++){
			        	if(fieldsValue[i] != selectedXaxisID){
			        		yaxis.addOption({value: fieldsValue[i], text: fieldsText[i]});
			        	}
			        }

			        yaxis.refreshOptions();
			        yaxis.enable();
		        }
			}

			function getXYDependencies(selectedXaxisID,selectedYaxisID,datefrom,dateto){
				$.get("yaxis-xaxis-bargraph-data.php",
		        {
		            yaxis_param: selectedYaxisID,
		            xaxis_param: selectedXaxisID,
		            datefrom: datefrom,
			        dateto: dateto,
		        },
		        function(data){
		        	var data = eval("(" + data + ")");
		        	xaxis = [];
		        	yaxis = [];
		        	xydata = [];
		        	/*Here I'm gonna process the data in order to slice it to parts
		        	In order to be easily passed to the Chart Procedure.*/
		        	for (var i=0;i<data.length;i++){
		        		var insertedXdata = false;
		        		var insertedYdata = false;
		        		if (xaxis.indexOf(data[i][selectedXaxisID]) == -1){
		        			xaxis.push(data[i][selectedXaxisID]);
		        			insertedXdata = true;
		        		}
		        		if (yaxis.indexOf(data[i][selectedYaxisID]) == -1){
		        			yaxis.push(data[i][selectedYaxisID]);
		        			insertedYdata = true;
		        		}

		        		if (insertedXdata){
		        			if (insertedYdata){
		        				var temp = [];
		        				for (var j=0;j<yaxis.length-1;j++){
		        					temp.push("0");
		        				}
		        				temp.push(data[i]["cnt"]);
		        				for (var j=0;j<xydata.length;j++){
		        					xydata[j].push("0");
		        				}
		        				xydata.push(temp);
		        			}else{
		        				var idxOfY = yaxis.indexOf(data[i][selectedYaxisID]);
		        				var temp = [];
		        				for (var j=0;j<yaxis.length;j++){
		        					if (j == idxOfY){
		        						temp.push(data[i]["cnt"]);
		        					}
		        					else{
		        						temp.push("0");
		        					}
		        				}
		        				xydata.push(temp);
		        			}
		        			
		        		}else{
		        			if (insertedYdata){
		        				var idxOfX = xaxis.indexOf(data[i][selectedXaxisID]);
		        				for (var j=0;j<xydata.length;j++){
		        					if (j == idxOfX){
		        						xydata[j].push(data[i]["cnt"]);
		        					}
		        					else{
		        						xydata[j].push("0");
		        					}
		        				}
		        			}else{
		        				var idxOfX = xaxis.indexOf(data[i][selectedXaxisID]);
		        				var idxOfY = yaxis.indexOf(data[i][selectedYaxisID]);
		        				xydata[idxOfX][idxOfY] = data[i]["cnt"];
		        			}
		        		}
		        	}
		        	drawGraph(xaxis,yaxis,xydata);
		        });
			}

			function prepareDataOfGraph(){
				/*On Changing Y-Axis or Date, It's supposed to fetch Y-axis dependencies in case that you
				want to display certain Y-axis Value, Also another ajax request will be fired which
				will get the relation between X-Axis and Y-Axis regarding to items count*/
				if (window.myBar != null){
					window.myBar.destroy();
				}

				var datefrom,dateto;
				datefrom = $('#datepicker-from').val();
				dateto = $('#datepicker-to').val();

				var yaxisval = $('#select-yaxis-val')[0].selectize;
		        yaxisval.disable();

		        var yaxisSelector = $('#select-yaxis')[0].selectize;
		        var selectedYaxisID = yaxisSelector.getValue();

		        var xaxisSelector = $('#select-xaxis')[0].selectize;
		        var selectedXaxisID = xaxisSelector.getValue();

		        if (selectedYaxisID != ""){
		        	/*This is the ajax that is responsible of getting Y-Axis selector data
		        	For Example: If you selected the name as your y-axis field then this ajax
		        	is obligated to fetch all names in the DB.*/
		        	$.get("yaxis-values-based-on-xaxis.php",
			        {
			            yaxis_param: selectedYaxisID,
			        },
			        function(data){
			        	var data = eval("(" + data + ")");
			        	yaxisval.clear();
			        	yaxisval.clearOptions();
			        	for(var i=0;i<data.length;i++){
			        		yaxisval.addOption({value: data[i][selectedYaxisID], text: data[i][selectedYaxisID]});
			        	}
			        	yaxisval.refreshOptions();
				        yaxisval.enable();
			        });

		        	/*This is the ajax that is responsible of X-Axis and Y-Axis relation in a
		        	numerical representation, I.E. How much is the frequency of X-Axis Regarding
		        	Y-Axis Field.*/
		        	getXYDependencies(selectedXaxisID,selectedYaxisID,datefrom,dateto);
		        }
			}

		    function onYaxisValChange(){
		    	/*Not Developed Yet*/
		    }
		</script>
	</head>

	<body>
		<div class="container justify-content-center">
			<h1 style="text-align: center;">Welcome To Campaign Graph Tool</h1>
		</div>
		<div class="container">
			<!-- Content here -->
			<div class="row justify-content-center">
				<div class="col-md-3">
					<select id="select-xaxis" name="Stats[xaxis]" onchange="onXaxisChange()">
						<option></option>
						<option value="name">Name</option>
						<option value="country">Country</option>
						<option value="category">Category</option>
						<option value="goal">Goal</option>
					</select>
				</div>
			</div>
			<br/>
			<div class="row justify-content-center">
				<div class="col-md-3">
					<select id="select-yaxis" name="Stats[yaxis]" onchange="prepareDataOfGraph()">
						<option></option>
					</select>
				</div>
				<div class="col-md-3">
					<select id="select-yaxis-val" name="Stats[yaxisval]">
						<option></option>
					</select>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-3">
					<input class="form-control" type="text" id="datepicker-from" placeholder="Date From" onchange="prepareDataOfGraph()">
				</div>
				<div class="col-md-3">
					<input class="form-control" type="text" id="datepicker-to" placeholder="Date To" onchange="prepareDataOfGraph()">
				</div>
			</div>
		</div>

		<div id="container" style="width: 75%;">
			<canvas id="canvas"></canvas>
		</div>
	</body>
</html>