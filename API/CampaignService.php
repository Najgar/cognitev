<?php
	class CampaignService{
		private $name,$country,$budget,$goal,$category;
		public function setName($name){
			$this->name = $name;
		}

		public function setCountry($country){
			$this->country = $country;
		}

		public function setBudget($budget){
			$this->budget = $budget;
		}

		public function setGoal($goal){
			$this->goal = $goal;
		}

		public function setCategory($category){
			$this->category = $category;
		}

		public function add(){
			include(__DIR__."/../DBFunction/CampaignTable.php");
			$model = new CampaignTable();
			return $model->insert(array("name"=>$this->name,"country"=>$this->country,"budget"=>$this->budget,"goal"=>$this->goal,"category"=>$this->category));
		}

		public function get($projection=null,$whereClause=null,$groupColumn=null){
			include(__DIR__."/../DBFunction/CampaignTable.php");
			$model = new CampaignTable();
			return $model->select($projection,$whereClause,$groupColumn);
		}
	}
?>